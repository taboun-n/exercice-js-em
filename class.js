class CanvasCircle {
  constructor(ctx, x, y, radius, fillColor) {
    this.ctx = ctx;
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.fillColor = fillColor;
  }

  draw() {
    this.ctx.beginPath();
    this.ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
    this.ctx.fillStyle = this.fillColor;
    this.ctx.fill();
    this.ctx.closePath();
  }

  move(x, y) {
    this.x = x;
    this.y = y;
  }

  collision(x, y) {
    return calcDist(this.x, this.y, x, y) < this.radius;
  }
}

class CanvasDisque extends CanvasCircle {
  constructor(ctx, x, y, radius, fillColor) {
    super(ctx, x, y, radius, fillColor);
  }

  draw() {
    this.ctx.beginPath();
    this.ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
    this.ctx.lineWidth = 10;
    this.ctx.strokeStyle = "black";
    this.ctx.stroke();
    this.ctx.fillStyle = this.fillColor;
    this.ctx.fill();
    this.ctx.closePath();
  }
}

class CanvasText {
  constructor(ctx, x, y, text) {
    this.ctx = ctx;
    this.x = x;
    this.y = y;
    this.text = text;
  }

  setText(text) {
    this.text = text;
  }

  draw() {
    this.ctx.font = "bold 20px sans-serif";
    this.ctx.fillStyle = "black";
    this.ctx.textAlign = "center";
    this.ctx.fillText(this.text, this.x, this.y);
  }
}