function calcDist(x, y, x1, x2) {
  const dx = x - x1;
  const dy = y - x2;
  const d = Math.sqrt(dx**2 + dy**2);

  return d;
}

function draw(ctx, width, height, disques, text, centerPoint) {
  ctx.clearRect(0, 0, width, height);

  let nearest = disques[0].fillColor;
  let d = calcDist(width/2, height/2, disques[0].x, disques[0].y);
  for (const disque of disques) {
    const dist = calcDist(width/2, height/2, disque.x, disque.y);
    if (dist < d) {
      d = dist;
      nearest = disque.fillColor;
    }

    disque.draw();
  }

  text.setText(nearest);
  text.draw();
  centerPoint.draw();
}

function getRealMousePos($canvas, ev) {
  const rect = $canvas.getBoundingClientRect();
  
  return {
    x: ev.clientX - rect.left,
    y: ev.clientY - rect.top
  };
}

function init($canvas) {
  if (!$canvas.getContext) return;

  const width = $canvas.width;
  const height = $canvas.height;
  const ctx = $canvas.getContext("2d");

  const centerPoint = new CanvasCircle(ctx, width/2, height/2, 25, "black");
  const text = new CanvasText(ctx, width/2, 100, "red");
  const disques = [];
  let draggle = null;

  disques.push(new CanvasDisque(ctx, 100, 100, 50, "red"));
  disques.push(new CanvasDisque(ctx, 150, height-100, 50, "blue"));
  disques.push(new CanvasDisque(ctx, width-100, height/2, 50, "yellow"));

  draw(ctx, width, height, disques, text, centerPoint);

  document.body.addEventListener("mousedown", function (ev) {
    const mousePos = getRealMousePos($canvas, ev);

    for (const disque of disques) {
      if (disque.collision(mousePos.x, mousePos.y)) {
        draggle = disque;
      }
    }
  });

  document.body.addEventListener("mousemove", (ev) => {
    if (draggle) {
      const mousePos = getRealMousePos($canvas, ev);
      draggle.move(mousePos.x, mousePos.y);
      draw(ctx, width, height, disques, text, centerPoint);
    }
  });

  document.body.addEventListener("mouseup", (ev) => {
    draggle = null;
  });
}

init(document.getElementById('exercice'));
